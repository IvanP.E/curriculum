# CURRICULUM VITAE

![foto](f1.png)

----
**Nom**: Ivan Pérez Espinosa

**Adreça**: C/Piferrer 106-110

**Telèfon**: 652 04 91 33

**Mail**: l43581017@outlook.es

----
## Actualment
**Estudiant**: Cursant 1er DAM

----
## Estudis
**ESO**: [Institut Princep de Viana](https://agora.xtec.cat/iespviana/)

**GM SMIX**: [Institut Poblenou](http://virtual.ecaib.org/)

----
## Idiomes
Idioma | Comprensió oral | Comprensió escrita | Expressió oral | Expressió escrita 
---|---|---|---|---
Català | molt bo | molt bo | molt bo | molt bo
Castellà | molt bo | molt bo | molt bo | molt bo
Anglès |  bo |  bo |  bo |  bo 

----
## Hobbies

> Sóc nedador i m'agrada veure i jugar a futbol

## Capacitats transversals

- Treball en equip
- Autònom
- Sociable
- Treballador
- Responsable

## Futur i aspiracions

M'agradaria continuar els meus estudis:
- [ ] Treure'm el 1r de SMIX.
- [x] Realitzar el cicle formatiu de grau superior de DAM.
- [x] Treure'm el carnet de conduir
- [ ] Obtenir un certificat d'anglès